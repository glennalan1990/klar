package clicommand_test

import (
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"

	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/clicommand"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/environment"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/internal/testutils"
)

func init() {
	*clicommand.CurrentScanTimeP = func() issue.ScanTime {
		timeFormat := "2006-01-02T15:04:05"
		mockedTime := "2020-01-25T15:04:05"

		parsedTime, _ := time.Parse(timeFormat, mockedTime)
		return issue.ScanTime(parsedTime)
	}
}

func TestAnalyze(t *testing.T) {
	t.Run("when the docker image is provided", func(t *testing.T) {
		os.Setenv(environment.EnvVarDockerImage, testutils.DefaultDockerImage)
		defer func() { testutils.UnsetEnvVars(t) }()

		flagSet := testutils.TestFlagSet("", "")

		c := cli.NewContext(nil, flagSet, nil)

		err := clicommand.Analyze(c)
		if err != nil {
			t.Errorf("Expected no err: %s", err)
		}
	})

	t.Run("when the docker image is not provided", func(t *testing.T) {
		os.Unsetenv(environment.EnvVarDockerImage)
		flagSet := testutils.TestFlagSet("", "")
		c := cli.NewContext(nil, flagSet, nil)

		err := clicommand.Analyze(c)

		expectedErrStr := "a Docker image value must be provided"

		if err == nil {
			t.Fatalf("Expected error string '%s', received no err", expectedErrStr)
		}

		if err.Error() != expectedErrStr {
			t.Errorf("Expected error string '%s', received '%s'", expectedErrStr, err.Error())
		}
	})
}

func TestAnalyzeAndConvert(t *testing.T) {
	var cases = []struct {
		Name, PathToAllowlistFile, PathToDockerfile, PathToExpectedFile, KlarReportFixture string
	}{
		{"when all vulnerabilities have been allowlisted",
			"clair-everything-whitelisted.yml", "", "common-report-empty.json", ""},
		{"when all vulnerabilities have been allowlisted with new file format",
			"clair-everything-allowlisted.yml", "", "common-report-empty.json", ""},
		{"when all but a single vulnerability have been allowlisted and a Dockerfile exists",
			"clair-whitelist.yml", testutils.PathToDockerfile(), "common-report-with-one-unapproved-cve.json", ""},
		{"when all but a single vulnerability have been allowlisted and a Dockerfile exists with new file format",
			"vulnerability-allowlist.yml", testutils.PathToDockerfile(), "common-report-with-one-unapproved-cve.json", ""},
		{"when no allowlist or Dockerfile exists", "", "",
			"common-report-with-all-unapproved-cves-no-remediations.json", ""},
		{"when no allowlist exists but a Dockerfile does exist", "", testutils.PathToDockerfile(),
			"common-report-with-all-unapproved-cves.json", ""},
		{"when an unresolvable CVE exists", "", testutils.PathToDockerfile(),
			"common-report-with-unresolvable-cve.json", "klarmock-with-unresolvable-cve.sh"},
	}

	for _, tc := range cases {
		t.Run(tc.Name, func(t *testing.T) {
			os.Setenv(environment.EnvVarDockerImage, testutils.DefaultDockerImage)
			defer func() { testutils.UnsetEnvVars(t) }()

			tempDir := testutils.TempDir()
			defer testutils.CleanUp()

			flagSet := testutils.TestFlagSet(tempDir, tc.KlarReportFixture)
			if tc.PathToAllowlistFile != "" {
				flagSet.String("allowlist", testutils.FixturesPath(tc.PathToAllowlistFile), "")
			}

			c := cli.NewContext(nil, flagSet, nil)

			flagSet.String("dockerfile-path", tc.PathToDockerfile, "")

			err := clicommand.AnalyzeAndConvert(c)
			if err != nil {
				t.Fatalf("Expected no err, received err: %s", err)
			}

			pathToActualFile := filepath.Join(tempDir, command.ArtifactNameContainerScanning)
			pathToExpectedFile := testutils.FixturesPath(tc.PathToExpectedFile)

			success, actualContent, expectedContent, err := testutils.CompareJSONFiles(
				pathToActualFile, pathToExpectedFile)
			if err != nil {
				t.Fatalf("Error encountered while comparing JSON with file: %s", err)
			}

			if !success {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", expectedContent, actualContent)
			}
		})
	}

	t.Run("when the docker image is not provided", func(t *testing.T) {
		os.Unsetenv(environment.EnvVarDockerImage)
		flagSet := testutils.TestFlagSet("", "")
		c := cli.NewContext(nil, flagSet, nil)

		err := clicommand.AnalyzeAndConvert(c)

		expectedErrStr := "a Docker image value must be provided"

		if err == nil {
			t.Fatalf("Expected error string '%s', received no err", expectedErrStr)
		}

		if err.Error() != expectedErrStr {
			t.Errorf("Expected error string '%s', received '%s'", expectedErrStr, err.Error())
		}
	})
}
