#!/bin/sh

# Cleanup
rm -f gl-container-scanning-report.json

# Run Container Scanning tool on a vulnerable webgoat image
BUILT_IMAGE=${BUILT_IMAGE:-container-scanning}

# need to get the IP address of the localhost so the clair server running in the docker
# image can access the clair-db postgres server running on localhost
CLAIR_DB_CONNECTION_STRING=${CLAIR_DB_CONNECTION_STRING:-"postgresql://postgres:password@$(getent hosts docker | awk '{ print $1 }'):5432/postgres?sslmode=disable&statement_timeout=60000"}

set -x

# build the image locally if not in the pipeline
if [[ "$BUILT_IMAGE" = "container-scanning" ]]; then
  docker build --pull -t ${BUILT_IMAGE} .
fi

docker run -p 5432:5432 -d --name clair-db arminc/clair-db@sha256:eee498fc6a059ec49b67c24f0b707f663e242418dc1b5150561cc8389ab11900

PROJECT_DIR=/tmp/project

docker run \
  --interactive --rm \
  --volume "$PWD":$PROJECT_DIR \
  -e CI_PROJECT_DIR=$PROJECT_DIR -e CLAIR_DB_CONNECTION_STRING \
  -e DOCKERFILE_PATH=$PROJECT_DIR/testdata/Dockerfile \
  -e ALLOWLIST_PATH=$PROJECT_DIR/testdata/integration-test-allowlist.yml \
  -e DOCKER_IMAGE=registry.gitlab.com/gitlab-org/security-products/dast/webgoat-8.0@sha256:bc09fe2e0721dfaeee79364115aeedf2174cce0947b9ae5fe7c33312ee019a4e \
  ${BUILT_IMAGE}

docker stop clair-db && docker rm clair-db

# Compare results
diff -u gl-container-scanning-report.json  test/expect/gl-container-scanning-report.json

if [ $? != 0 ] ; then
  echo "Analyze results differ from expectations"
  exit 1
else
  echo "Analyze results match expected results"
fi
