package clair

// Export for testing
var (
	ClairRetryWaitP                 = &clairRetryWait
	VulnerabilitiesDBRetryWaitP     = &vulnerabilitiesDBRetryWait
	MaxVulnerabilitiesDBRetryCountP = &maxVulnerabilitiesDBRetryCount
	MaxClairServerRetryCountP       = &maxClairServerRetryCount
	PathToClairBinaryP              = &pathToClairBinary
	PathToConfigFileTemplateP       = &pathToConfigFileTemplate
	PathToConfigFileP               = &pathToConfigFile
	WriteConfigFile                 = writeConfigFile
	ClairServerArgs                 = clairServerArgs
)
