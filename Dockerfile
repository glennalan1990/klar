# Copyright 2017 clair authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ARG CLAIR_REPO_TAG=v2.1.4
ARG KLAR_EXECUTABLE_VERSION=2.4.0
ARG KLAR_EXECUTABLE_SHA=09764983d4e9a883754b55b16edf5f0be558ab053ad6ee447aca0199ced3d09f

ARG GO_VERSION=1.14
FROM golang:$GO_VERSION-alpine AS builder

ARG CLAIR_REPO_TAG
ARG KLAR_EXECUTABLE_VERSION
ARG KLAR_EXECUTABLE_SHA

RUN apk add --no-cache --update git xz
COPY clair-config.yaml.template /

# build the GitLab container scanning tool
# TODO: should we just move all of the go support files to klar-analyzer?
RUN mkdir /klar-analyzer
COPY . /klar-analyzer/
WORKDIR /klar-analyzer
RUN go build -o /analyzer

WORKDIR /go/src/github.com/coreos/clair/

RUN git clone --branch $CLAIR_REPO_TAG https://github.com/coreos/clair /go/src/github.com/coreos/clair/ && \
    wget https://github.com/optiopay/klar/releases/download/v${KLAR_EXECUTABLE_VERSION}/klar-${KLAR_EXECUTABLE_VERSION}-linux-amd64 \
      -O /klar && \
    echo "${KLAR_EXECUTABLE_SHA}  /klar" | sha256sum -c && \
    chmod +x /klar && \
    export CLAIR_VERSION=$(git describe --always --tags --dirty) && \
    go install -ldflags "-X github.com/coreos/clair/pkg/version.Version=$CLAIR_VERSION" -v github.com/coreos/clair/cmd/clair && \
    mv /go/bin/clair /clair

FROM alpine:3.11.3

ARG CLAIR_REPO_TAG
ARG KLAR_EXECUTABLE_VERSION
ENV CLAIR_REPO_TAG $CLAIR_REPO_TAG
ENV KLAR_EXECUTABLE_VERSION $KLAR_EXECUTABLE_VERSION

# we only want to start the clair server if we're running within Docker
ENV START_CLAIR_SERVER true

# clair depends on git and xz
COPY --from=builder /usr/bin/git /usr/bin/xz /usr/bin/

COPY --from=builder clair-config.yaml.template /clair /klar /analyzer /
COPY container-scanner /container-scanner

# - ca-certificates are needed in order for clair to pull images from Docker repositories.
# - dumb-init acts as PID 1 and immediately spawns the analyze command as a child process,
#   taking care to properly handle and forward signals as they are received.
# - rpm is required to allow clair to scan redhat based images.
RUN apk --no-cache --update add ca-certificates dumb-init rpm && \
  # alpine images don't include an /etc/nsswitch.conf file. It's necessary to manually create
  # one, otherwise Go-based binaries won't resolve hosts via /etc/hosts and will instead always
  # use a DNS lookup.  This means that referencing `localhost` in Go code might cause issues
  # in certain host configurations unless this /etc/nsswitch.conf file exists.
  # See https://github.com/gliderlabs/docker-alpine/issues/367#issuecomment-351919742
  # and https://github.com/golang/go/issues/22846#issuecomment-346377144 for more details
  [ ! -e /etc/nsswitch.conf ] && echo 'hosts: files dns' > /etc/nsswitch.conf

WORKDIR /

CMD ["/usr/bin/dumb-init", "/container-scanner/start.sh"]