package convert

import (
	"io"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/remediate"
)

// Convert transforms a list of vulnerabilities from klar (https://github.com/optiopay/klar)
// format to the Security Products Common Format without applying any allowlisting.  It also
// generates a list of remediations if there are fixes available for the given vulnerabilities
func Convert(klarJSONReport io.Reader, dockerImageName, pathToDockerfile string) (*issue.Report, error) {
	klarVulnerabilities, err := parseKlarVulnerabilities(klarJSONReport)
	if err != nil {
		return nil, err
	}

	remediationErrEncountered := false

	remediator, err := remediate.NewRemediator(pathToDockerfile)
	if err != nil {
		log.Warnf("Encountered error while reading Dockerfile for remediation, halting remediation processing. Error: %s", err)
		remediationErrEncountered = true
	}

	issues := []issue.Issue{}
	remediations := []issue.Remediation{}

	for _, v := range klarVulnerabilities {
		r := result{v, dockerImageName, remediator}

		issues = append(issues, *r.toIssue())

		if !remediationErrEncountered {
			addRemediation(&remediations, r)
		}
	}

	report := issue.NewReport()
	report.Vulnerabilities = issues
	report.Remediations = remediations
	report.Scan.Scanner = metadata.ReportScanner
	report.Scan.Type = metadata.Type

	return &report, nil
}

func addRemediation(remediations *[]issue.Remediation, r result) {
	remediation, err := r.toRemediation()
	if err != nil {
		log.Warnf("Encountered error while processing remediation for issue %s, skipping. Error: %s",
			r.compareKey(), err)
		return
	}

	if remediation == nil {
		return
	}

	*remediations = append(*remediations, *remediation)
}

type result struct {
	klarVulnerability
	dockerImageName string
	remediator      *remediate.Remediator
}
