package reporter_test

import (
	"path/filepath"
	"reflect"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/internal/testutils"
	"gitlab.com/gitlab-org/security-products/analyzers/klar/v2/reporter"
)

func TestReportToFile(t *testing.T) {
	var cases = []struct {
		Name, PathToExpectedFile string
		Report                   *issue.Report
		UnapprovedCompareKeys    map[string]bool
	}{
		{
			"when there are only two unapproved CompareKeys", "common-report-with-two-unapproved-comparekeys.json",
			&issue.Report{
				Version: issue.CurrentVersion(),
				Vulnerabilities: []issue.Issue{
					{
						CompareKey:  "debian:10:nghttp2:CVE-2014-9513",
						Identifiers: []issue.Identifier{{Value: "CVE-2014-9513"}},
						Severity:    issue.SeverityLevelHigh,
					},
					{
						CompareKey:  "debian:10:nghttp2:CVE-2012-9513",
						Identifiers: []issue.Identifier{{Value: "CVE-2012-9513"}},
						Severity:    issue.SeverityLevelHigh,
					},
					{
						CompareKey:  "debian:10:nghttp2:CVE-2018-9510",
						Identifiers: []issue.Identifier{{Value: "CVE-2018-9510"}},
						Severity:    issue.SeverityLevelUnknown,
					},
					{
						CompareKey:  "debian:10:nghttp2:CVE-2016-9513",
						Identifiers: []issue.Identifier{{Value: "CVE-2016-9513"}},
						Severity:    issue.SeverityLevelMedium,
					},
				},
			},
			map[string]bool{"debian:10:nghttp2:CVE-2018-9510": true, "debian:10:nghttp2:CVE-2012-9513": true},
		},
		{
			"when remediations exist", "common-report-with-remediations.json",
			&issue.Report{
				Version: issue.CurrentVersion(),
				Vulnerabilities: []issue.Issue{
					{
						CompareKey:  "centos:6:vim-minimal:RHSA-2016:2972",
						Identifiers: []issue.Identifier{{Value: "RHSA-2016:2972"}},
						Severity:    issue.SeverityLevelHigh,
					},
					{
						CompareKey:  "debian:10:openssl:CVE-2014-9678",
						Identifiers: []issue.Identifier{{Value: "CVE-2014-9678"}},
						Severity:    issue.SeverityLevelHigh,
					},
					{
						CompareKey:  "debian:10:nghttp2:CVE-2014-9513",
						Identifiers: []issue.Identifier{{Value: "CVE-2014-9513"}},
						Severity:    issue.SeverityLevelLow,
					},
				},
				Remediations: []issue.Remediation{
					{
						Fixes: []issue.Ref{
							{
								CompareKey: "centos:6:vim-minimal:RHSA-2016:2972",
							},
						},
						Summary: "Upgrade vim-minimal from 2:7.4.629-5.el6 to 2:7.4.629-5.el6_8.1",
						Diff:    "LS0tIGEvdGVzdGRhdGEvRG9ja2VyZmlsZQorKysgYi90ZXN0ZGF0YS9Eb2NrZXJmaWxlCkBAIC0zNCw2ICszNCw3IEBACiBSVU4gZ28gYnVpbGQgLW8gL2FuYWx5emVyCiBXT1JLRElSIC9nby9zcmMvZ2l0aHViLmNvbS9jb3Jlb3MvY2xhaXIvCiBGUk9NIHJlZ2lzdHJ5LmdpdGxhYi5jb20vZ2l0bGFiLW9yZy9zZWN1cml0eS1wcm9kdWN0cy9kYXN0L3dlYmdvYXQtOC4wQHNoYTI1NjpiYzA5ZmUyZTA3MjFkZmFlZWU3OTM2NDExNWFlZWRmMjE3NGNjZTA5NDdiOWFlNWZlN2MzMzMxMmVlMDE5YTRlCitSVU4geXVtIC15IGNoZWNrLXVwZGF0ZSB8fCB7IHJjPSQ/OyBbICIkcmMiIC1uZXEgMTAwIF0gJiYgZXhpdCAkcmM7IHl1bSB1cGRhdGUgLXkgdmltLW1pbmltYWw7IH0KICMgdXNlZCB0byBkZXRlcm1pbmUgd2l0aGluIHRoZSBjb250YWluZXIgc2Nhbm5pbmcgYmluYXJ5IHdoZXRoZXIgd2UncmUgcnVubmluZwogIyBmcm9tIHdpdGhpbiB0aGUgY29udGV4dCBvZiBEb2NrZXIKIEVOViBSVU5OSU5HX0ZST01fRE9DS0VSICJ0cnVlIgo=",
					},
				},
			},
			map[string]bool{"centos:6:vim-minimal:RHSA-2016:2972": true, "debian:10:openssl:CVE-2014-9678": true},
		},
	}

	for _, tc := range cases {
		t.Run(tc.Name, func(t *testing.T) {
			tempDir := testutils.TempDir()
			defer testutils.CleanUp()

			err := reporter.ReportToFile(tc.Report, tc.UnapprovedCompareKeys, tempDir)
			if err != nil {
				t.Errorf("Expected no err: %s", err)
			}

			pathToActualFile := filepath.Join(tempDir, command.ArtifactNameContainerScanning)
			pathToExpectedFile := testutils.FixturesPath(tc.PathToExpectedFile)

			success, actualContent, expectedContent, err := testutils.CompareJSONFiles(
				pathToActualFile, pathToExpectedFile)
			if err != nil {
				t.Errorf("Error encountered while comparing JSON with file: %s", err)
			}

			if !success {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", expectedContent, actualContent)
			}
		})
	}
}

func TestExtIDForVulnerability(t *testing.T) {
	t.Run("when an identifier exists", func(t *testing.T) {
		vulnerability := issue.Issue{
			CompareKey:  "debian:10:nghttp2:CVE-2014-9513",
			Identifiers: []issue.Identifier{{Value: "CVE-2014-9513"}},
			Severity:    issue.SeverityLevelHigh,
		}

		want := "CVE-2014-9513"
		got := reporter.ExtIDForVulnerability(vulnerability)

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})

	t.Run("when an identifier does not exist", func(t *testing.T) {
		vulnerability := issue.Issue{
			CompareKey:  "oracle:5:glibc-common:UNKNOWN-2017-1479",
			Identifiers: []issue.Identifier{},
			Severity:    issue.SeverityLevelHigh,
		}

		want := "oracle:5:glibc-common:UNKNOWN-2017-1479"
		got := reporter.ExtIDForVulnerability(vulnerability)

		if !reflect.DeepEqual(want, got) {
			t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
		}
	})
}
